Installation of *gwien2k*
=========================

Prerequisites
-------------

*cygutz* is a Gutzwiller-Rotationally Invariant Slave-Boson package, 
written in Fortran90 and c.
The recommended operating system, packages and libraries are:

* Linux

* Intel family: ifort, icc, icpc, mkl 
  and intel-mpi (mpiifort, mpiicc, mpiicpc)

* Parallel-HDF5 library.
  To configure, using line like

  ./configure --prefix=YOUR_PREFIX CC=mpiicc FC=mpiifort CXX=mpiicpc 
  --enable-fortran --enable-fortran2003 --enable-parallel

  Check for details at 
  https://portal.hdfgroup.org/display/support/Documentation.

Build and install
-----------------

The compilation and installation consist of three steps: 

* Download *cygutz* here at 
  https://gitlab.com/yxphysics/gwien2k/-/archive/master/gwien2k-master.tar.gz, 
  unzip it and cd to the directory (``gwien2k-master``). 
  You may type::

  $ wget https://gitlab.com/yxphysics/cygutz/-/archive/master/cygutz-master.tar.gz 
  $ tar -xzf cygutz-master.tar.gz
  $ cd cygutz-master

* Choose a ``Makefile.in`` file in directory ``template_makefile.in``. 
  Modify ``DESTDIR`` to the `bin` directory of *gwien2k* or *comrisb*. 
  For example, you may type::

  $ cp template_makefile.in/Makefile.in.parallel_hdf5_intel Makefile.in
  $ vi ./Makefile.in

* Type `make` to install it.

Additional requirements for *wien2k+G* calculations
---------------------------------------------------
Besides the interface, users need to 

* purchase and install *wien2k* (http://susi.theochem.tuwien.ac.at/).

* instal *pygrisb*: a python lib and driver for *DFT+G-RISB* calculations.
  Follow the following steps to install a local package::

  $ wget https://gitlab.com/yxphysics/pygrisb/-/archive/master/pygrisb-master.tar.gz
  $ tar -xzf pygrisb-master.tar.gz
  $ cd pygrisb-master
  $ make

* install *gwien2k*. https://gitlab.com/yxphysics/gwien2k.
  Follow instructions in `INSTALL.rst`.
