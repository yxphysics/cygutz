CyGutz: a Gutzwiller variational embedding approach (equivalent to rotationally invariant slave-boson method at saddle-point approximation) to solve the ground state properties of a generic Hubbard model. Used with a python wrapper pygrisb. It is a key component in ComRISB, a subpackage in Comsuite developed in the US DOE's Center for Computational Material Spectroscopy and Design at Brookhaven National Laboratory.

To cite this code, goto https://figshare.com/articles/cygutz/11987439 to get the file for your need(e.g. BibTex, Refworks, etc). Thanks!
