program test_gtime
use gtime
implicit none

type(time_ob) :: time

call time%start(1)
write(*,*)"press any key when done with the test."
read(*,*)
call time%finish(1)
call time%print_usage("test",1,6)


end program test_gtime
