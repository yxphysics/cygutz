include ./Makefile.in

install:
	#--------------------------------------------------------
	mkdir -p ${DESTDIR}
	if [ "${DESTDIR}" != "${WIEN_GUTZ_ROOT}" ]; \
        then \
      echo -e '\nexport WIEN_GUTZ_ROOT=${DESTDIR}' >> ~/.bashrc; \
    fi
	# Build Gutzwiller solver 
	cd src && make && make install && cd ..
	cp -r ./pygtool/*  ${DESTDIR}

pygtools:
	cp -r ./pygtool/*  ${DESTDIR}

clean:
	cd src && make clean && cd ..
	rm -f ${DESTDIR}/Cy* ${DESTDIR}/exe_*
