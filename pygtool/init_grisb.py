#!/usr/bin/env python3
# init_ga for comrisb.
from pygrisb.gutz.init import screen_init


s_init = screen_init()
s_init.run()
