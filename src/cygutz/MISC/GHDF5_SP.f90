module ghdf5
    use gprec
    use ghdf5_base
    use sparse
    use hdf5
    implicit none
    private
    public :: gh5_write_sp_matrix

    contains

    subroutine gh5_write_sp_matrix(spmat, path, f_id)
    type(sp_matrix), target, intent(in) :: spmat
    character(len=*), intent(in) :: path
    integer(hid_t), intent(in) :: f_id

    integer nnz

    call gh5_gcreate(path, f_id, g_id=group_id)
    call gh5_write(spmat%nrow, "nrow", group_id, f_id)
    call gh5_write(spmat%ncol, "ncol", group_id, f_id)
    call gh5_write(1, "base", group_id, f_id)
    call gh5_gclose(group_id)
    select type(spmat)
    type is(dcsr_matrix)
        call gh5_write(dcsr%i, dcsr%nrow + 1, path//"/indptr", f_id)
        nnz = dcsr%i(dcsr%nrow + 1) -1
        call gh5_write(dcsr%j, nnz, path//"/indices", f_id)
        call gh5_write(dcsr%a, nnz, path//"/data", f_id)
    type is(zcsr_matrix)
        call gh5_write(zcsr%i, zcsr%nrow + 1, path//"/indptr", f_id)
        nnz = zcsr%i(zcsr%nrow + 1) -1
        call gh5_write(zcsr%j, nnz, path//"/indices", f_id)
        call gh5_write(zcsr%a, nnz, path//"/data", f_id)
    type is(dcoo_matrix)
        call gh5_write(dcoo%nnz, "nnz", group_id, f_id)
        call gh5_write(dcoo%i, dcoo%nnz, path//"/i", f_id)
        call gh5_write(dcoo%j, dcoo%nnz, path//"/j", f_id)
        call gh5_write(dcoo%a, dcoo%nnz, path//"/data", f_id)
    end select
    call gh5_gclose(group_id)
    return

    end subroutine gh5_write_sp_matrix


end module ghdf5
