!******************************************************************************
! Copyright c 2013, The Ames Laboratory, Iowa State University, and Rutgers
! University*.  All rights reserved.
!
! This software was authored by Yongxin Yao, Nicola Lanata*, Gabriel Kotliar*,
! Cai-Zhuang Wang, and Kai-Ming Ho, at The Ames Laboratory and
! Rutgers University and was supported by the U.S.
! Department of Energy (DOE), Office of Science,
! Basic Energy Sciences, Materials Science and Engineering Division.
! The Ames Laboratory is operated by Iowa State University for DOE
! under U.S. Government contract DE-AC02-07CH11358.
! The U.S. Government has the rights to use, reproduce, and
! distribute this software.
! NEITHER THE GOVERNMENT, THE AMES LABORATORY, IOWA STATE UNIVERSITY,
! NOR RUTGERS UNIVERSITY MAKES ANY WARRANTY,
! EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.
! If software is modified to produce derivative works,
! such modified software should be clearly marked,
! so as to not confuse it with the version available from
! The Ames Laboratory and Rutgers University.
!
! Additionally, redistribution and use in source and binary forms,
! with or without modification,
! are permitted provided that the following conditions are met:
!
!     Redistribution of source code must retain the above copyright notice,
!     this list of conditions, and the following disclaimer.
!
!     Redistribution in binary form must reproduce the above copyright notice,
!     this list of conditions, and the following disclaimer
!     in the documentation and/or other materials provided with distribution.
!
!     Neither the name of The Ames Laboratory, Iowa State University,
!     Rutgers University, the U.S. Government, nor the names of
!     its contributors may be used to endorse or promote products derived
!     from this software without specific prior written permission.
!
! THIS SOFTWARE IS PROVIDED BY THE AMES LABORATORY, IOWA STATE UNIVERSITY,
! RUTGERS UNIVERSITY, AND CONTRIBUTORS "AS IS"
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
! THE IMPLIED WARRANTIES OF MERCHANTABILITY
! AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
! IN NO EVENT SHALL THE GOVERNMENT, THE AMES LABORATORY,
! IOWA STATE UNIVERSITY, RUTGERS UNIVERSITY, OR CONTRIBUTORS BE LIABLE
! FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
! EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
! PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
! LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
! HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
! WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
! OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
! EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
!******************************************************************************

module localstore
    use gprec
    use corrorb
    use hdf5
    use ghdf5
    implicit none
    private

    type frozen
        integer :: nsorb,nelect
        integer,pointer :: idx_orb(:)
    end type frozen
      
    type matrix_basis
        integer :: dimhst=0,dimhsmax=0,dimhs1123
        !< dimension of the matrix basis for each atom
        integer,pointer :: dim_hs(:)
        complex(q),pointer :: hs(:)=>null() ! matrix basis
        integer,pointer :: m_struct(:)=>null() ! self-energy structure
    end type matrix_basis

    type,public :: localstore_ob
        integer :: num_imp,ntyp
        integer,allocatable :: ityp_list(:), & !< type of the impurity.
                &imap_list(:), & !< equiv. impurity map index. imap_list(i)<=i.
                &na2_list(:), & !< impurity spin-orbital dimension list
                !< Index to whether calculate it on local core.
                &local_imp(:),naso_imp(:), &
                &nval_bot_ityp(:),nval_top_ityp(:)
        real(q),allocatable :: et1(:),eu2(:)
        integer :: na2112,na2max,nasomax,nasotot
        integer :: iso,ispin,ispin_in,ispo,rspo,nspin,ivext=1
        type(corrorb_ob),allocatable::co(:)
        type(frozen),pointer::fz(:)

        real(q) :: ef,edcla1,symerr=0._q
        integer :: r_factor=2
        complex(q),pointer :: r(:),r0(:),d(:),d0(:),la1(:),la2(:),z(:), &
                &copy(:),nks(:),nc_var(:),nc_phy(:),h1e(:),isimix(:), &
                &vext(:)=>null(),&
                &sx(:)=>null(),sy(:)=>null(),sz(:)=>null(), &
                &lx(:)=>null(),ly(:)=>null(),lz(:)=>null(),r_coef(:), &
                ! additional rotations for bare hamiltonian.
                ! complex 
                &db2sab(:)=>null() ! complex spherical 
        real(q),pointer :: nks_coef(:),la1_coef(:),ncv_coef(:)
        !> hermitian matrix basis, (hermitian) maxtrix basis with
        !! selective mott localization.
        type (matrix_basis) :: hm, hm_l, hm_r

        contains
        procedure,public::init=>init_localstore

    end type localstore_ob

    contains


    subroutine init_localstore(this,gh5,io)
    class(localstore_ob)::this
    class(hdf5_ob)::gh5
    integer,intent(in)::io

    logical lexist
   
    call gh5%open_r('GParam.h5',gh5%f_id)
    ! open root group
    call gh5%gopen('/',gh5%f_id,gh5%group_id)
    !< Get num_imp, ityp_list, imap_list and na2_list
    call set_impurities_index(this,gh5)
    call write_impurities_index(io)
    call gh5_read_loc_hs(this,gh5,'matbs_dim_list','/matrix_basis', &
            &'/symbol_matrix',this%hm)
    call set_loc_db2sab(this,gh5,io)
    call set_loc_sl_vec(this,gh5)
    allocate(this%co(this%num_imp))
    call set_v2e_list(this,gh5,io)
    ! close root group
    call gh5%gclose(gh5%group_id)
    call gh5%close(gh5%f_id)

    inquire(file='GVExt.h5',exist=lexist)
    if(lexist)then
        call gh5%open_r('GVExt.h5',gh5%f_id)
        call set_loc_vext(this,gh5,io)
        call gh5%gopen("/",gh5%f_id,gh5%group_id)
        call gh5%read(this%ivext,'givext',gh5%group_id,gh5%f_id)
        call gh5%gclose(gh5%group_id)
        call gh5%close(gh5%f_id)
    endif

    inquire(file='GMott.h5',exist=lexist)
    if(lexist)call gh5%open_r('GMott.h5',gh5%f_id)
    call set_loc_hs(this,gh5,'matbs_r_dim_list',"/matrix_basis_r", &
            &"/symbol_matrix_r",this%hm_r,lexist)
    call set_loc_hs(this,gh5,'matbs_l_dim_list',"/matrix_basis_l", &
            &"/symbol_matrix_l",this%hm_l,lexist)
    call set_mott_localized_orbitals(this,gh5,lexist)
    if(lexist)call gh5%close(gh5%f_id)

    call output_mott_localized_orbitals(this,io)
    call alloc_localstore(this)
    call link_co_localstore()
    return

    end subroutine init_localstore


    !< Impurities should be ordered according to types.
    subroutine set_v2e_list(this,gh5,io)
    class(localstore_ob)::this
    class(hdf5_ob)::gh5
    integer,intent(in)::io

    integer i,imap,na2
    real(q),allocatable::v2e(:,:,:,:)

    do i=1,this%num_imp
        imap=this%imap_list(i)
        if(imap/=i)then
            this%co(i)%v2e=>this%co(imap)%v2e
            this%co(i)%v_j2e=>this%co(imap)%v_j2e
        else
            na2=this%na2_list(i)
            allocate(this%co(i)%v2e(na2,na2,na2,na2))
            ! v2e is real, while %v2e is complex
            if(i==1)then
                allocate(v2e(na2,na2,na2,na2))
            elseif(na2/=this%na2_list(i-1))then
                deallocate(v2e)
                allocate(v2e(na2,na2,na2,na2))
            endif
            call gh5%read(v2e,na2,na2,na2,na2,'/impurity_'// &
                    &trim(int_to_str(i-1))//'/v2e_db2',gh5%f_id)
            if(io>0)then
                write(io,'(" imp = ",I2," v2e(1,1,1,1) = ",f0.2)')i, &
                        &v2e(1,1,1,1)
            endif
            this%co(i)%v2e = v2e
            allocate(this%co(i)%v_j2e(na2,na2,na2,na2))
            call get_coul_exchange(na2,this%co(i)%v2e,this%co(i)%v_j2e)
        endif
    enddo
    deallocate(v2e)
    return

    end subroutine set_v2e_list


    subroutine calc_la1_hf_list()
    integer i

    do i=1,this%num_imp
        call calc_co_la1_hf(this%co(i))
    enddo
    return

    end subroutine calc_la1_hf_list


    subroutine link_co_localstore(this)
    class(localstore_ob)::this

    integer i,na2,na22,naso,nasoo,dim_hs,ibase,jbase

    ibase=0; jbase=0
    do i=1,this%num_imp
        na2=this%na2_list(i)
        na22=na2*na2
        this%co(i)%dim2=na2; this%co(i)%dim=na2/2
        naso=na2*this%iso/2
        this%co(i)%dimso=naso
        nasoo=this%co(i)%dimso**2
        this%co(i)%dim_hs_l = this%hm_l%dim_hs(i)
        this%co(i)%nks   (1:na2,1:na2) => this%nks   (ibase+1:ibase+na22)
        this%co(i)%isimix(1:na2,1:na2) => this%isimix(ibase+1:ibase+na22)
        this%co(i)%nc_var(1:na2,1:na2) => this%nc_var(ibase+1:ibase+na22)
        this%co(i)%nc_phy(1:na2,1:na2) => this%nc_phy(ibase+1:ibase+na22)
        this%co(i)%r     (1:na2,1:na2) => this%r     (ibase+1:ibase+na22)
        this%co(i)%r0    (1:na2,1:na2) => this%r0    (ibase+1:ibase+na22)
        this%co(i)%z     (1:na2,1:na2) => this%z     (ibase+1:ibase+na22)
        this%co(i)%d0    (1:na2,1:na2) => this%d0    (ibase+1:ibase+na22)
        this%co(i)%d     (1:na2,1:na2) => this%d     (ibase+1:ibase+na22)
        this%co(i)%la1   (1:na2,1:na2) => this%la1   (ibase+1:ibase+na22)
        this%co(i)%la2   (1:na2,1:na2) => this%la2   (ibase+1:ibase+na22)
        this%co(i)%h1e   (1:na2,1:na2) => this%h1e   (ibase+1:ibase+na22)

        if(associated(this%vext))then
            this%co(i)%vext(1:na2,1:na2) => this%vext(ibase+1:ibase+na22)
        endif

        if(associated(this%sx))then
            this%co(i)%sx(1:na2,1:na2) => this%sx(ibase+1:ibase+na22)
            this%co(i)%sy(1:na2,1:na2) => this%sy(ibase+1:ibase+na22)
        endif
        if(associated(this%sz))then
            this%co(i)%sz(1:na2,1:na2) => this%sz(ibase+1:ibase+na22)
        endif
        if(associated(this%lx))then
            this%co(i)%lx(1:na2,1:na2) => this%lx(ibase+1:ibase+na22)
            this%co(i)%ly(1:na2,1:na2) => this%ly(ibase+1:ibase+na22)
        endif
        if(associated(this%lz))then
            this%co(i)%lz(1:na2,1:na2) => this%lz(ibase+1:ibase+na22)
        endif
        if(associated(this%db2sab))then
            this%co(i)%db2sab(1:na2,1:na2) => &
                    &this%db2sab(ibase+1:ibase+na22)
        endif

        this%co(i)%m_struct(1:na2,1:na2) => &
                &this%hm%m_struct(ibase+1:ibase+na22)

        dim_hs=this%hm_l%dim_hs(i)
        if(dim_hs>0)then
            this%co(i)%hs_l(1:na2,1:na2,1:dim_hs) => this%hm_l%hs(jbase+1: &
                    &jbase+na22*dim_hs)
        endif
        jbase=jbase+na22*dim_hs
        ibase=ibase+na22
    enddo
    return

    end subroutine link_co_localstore


    ! \sum_ij \lambda_ij <c^\dagger_i c_j>
    subroutine calc_edcla1()
    
    this%edcla1=real(sum(this%la1*this%nks),q)
    return

    end subroutine calc_edcla1


    subroutine set_loc_hs_dimtot(this,mb)
    class(localstore_ob)::this
    type(matrix_basis),intent(inout)::mb

    integer i

    mb%dimhsmax=maxval(mb%dim_hs)
    mb%dimhst=0
    mb%dimhs1123=0
    do i=1,this%num_imp
        mb%dimhs1123=mb%dimhs1123+this%na2_list(i)**2*mb%dim_hs(i)
        if(this%imap_list(i)==i)then
            mb%dimhst=mb%dimhst+mb%dim_hs(i)
        endif
    enddo
    return

    end subroutine set_loc_hs_dimtot


    subroutine set_impurities_index(this,gh5)
    class(localstore)::this
    class(hdf5_ob)::gh5

    logical lexist
    integer err

    ! read attributes 
    call gh5%read(this%iso, 'iso', gh5%group_id, gh5%f_id)
    call gh5%read(this%ispin, 'ispin', gh5%group_id, gh5%f_id)
    call gh5%read(this%num_imp, 'num_imp', gh5%group_id, gh5%f_id)
    allocate(this%ityp_list(this%num_imp), this%imap_list(this%num_imp), &
            &this%na2_list(this%num_imp),this%naso_imp(this%num_imp))
    call gh5%read(this%ityp_list, this%num_imp, 'ityp_list', &
            &gh5%group_id, gh5%f_id)
    call gh5%read(this%imap_list, this%num_imp, 'imap_list', &
            &gh5%group_id, gh5%f_id)
    call gh5%read(this%na2_list, this%num_imp, 'na2_list', &
            &gh5%group_id, gh5%f_id)

    ! zero-base to one-base
    this%ityp_list = this%ityp_list + 1
    this%imap_list = this%imap_list + 1

    this%ispo=max(this%iso,this%ispin)
    this%rspo=3-this%ispo
    this%nspin=max(1,this%ispin/this%iso)
    this%na2max=maxval(this%na2_list)
    this%nasomax=this%na2max*this%iso/2
    this%na2112=sum(this%na2_list**2)
    this%nasotot=sum(this%na2_list)*this%iso/2
    this%naso_imp=this%na2_list*this%iso/2
    this%ntyp=maxval(this%ityp_list)

    call h5aexists_f(gh5%group_id,'nval_bot_list', lexist, err)
    if(lexist)then
        allocate(this%nval_bot_ityp(this%ntyp),this%nval_top_ityp(this%ntyp))
        call gh5%read(this%nval_bot_ityp, this%ntyp, 'nval_bot_list', &
                &gh5%group_id, gh5%f_id)
        call gh5%read(this%nval_top_ityp, this%ntyp, 'nval_top_list', &
                &gh5%group_id, gh5%f_id)
    endif
    return

    end subroutine set_impurities_index


    subroutine wh_set_local_imp(local_imp)
    integer,intent(in)::local_imp(this%num_imp)

    allocate(this%local_imp(this%num_imp))
    this%local_imp=local_imp
    return

    end subroutine wh_set_local_imp


    subroutine write_impurities_index(io)
    integer,intent(in)::io

    if(io<0)return
    write(io,'(" total number of impurities = ",I4)')this%num_imp
    write(io,'(" impurity type indices:")')
    write(io,'(4x,10I4)')this%ityp_list
    write(io,'(" impurity imap indices:")')
    write(io,'(4x,10I4)')this%imap_list
    write(io,'(" impurity num_spin_orbitals:")')
    write(io,'(4x,10I4)')this%na2_list
    return

    end subroutine write_impurities_index


    subroutine alloc_localstore(this)
    class(localstore_ob)::this
      
    allocate(this%r(this%na2112), this%r0(this%na2112), &
            &this%z(this%na2112), this%d(this%na2112), &
            &this%d0(this%na2112), this%la1(this%na2112), &
            &this%la2(this%na2112), this%nks(this%na2112), &
            &this%isimix(this%na2112), this%nc_var(this%na2112), &
            &this%nc_phy(this%na2112), this%copy(this%na2112), &
            &this%h1e(this%na2112), &
            &this%nks_coef(this%hm_l%dimhst),this%la1_coef(this%hm_l%dimhst), &
            &this%ncv_coef(this%hm_l%dimhst),this%r_coef(this%hm_r%dimhst), &
            &this%et1(this%num_imp), this%eu2(this%num_imp))
    this%r=0; this%r0=0; this%z=0; this%d=0; this%d0=0; this%la1=0; this%la2=0
    this%nks=0; this%isimix=0; this%nc_var=0; this%nc_phy=0
    return
      
    end subroutine alloc_localstore
     

    subroutine init_wh_x(io,r_default)
    integer,intent(in)::io
    real(q),intent(in)::r_default

    logical lexist

    inquire(file='WH_RL_Inp.h5',exist=lexist)
    if(lexist)then
        call gh5_read_wh_rl()
    else
        this%la1=this%h1e
        call set_diagonal_r(r_default)
        if(this%ivext==0.and.associated(this%vext))then
            this%la1=this%la1+this%vext
        endif
    endif
    call modify_r_la1_frozen(0, 30._q)
    call calc_r_pp(io,'r-inp')
    call calc_la1_pp(io,'la1-inp')
    return

    end subroutine init_wh_x


    subroutine set_diagonal_r(r_default)
    real(q),intent(in)::r_default

    integer i,j

    do i=1,this%num_imp
        do j=1,this%na2_list(i)
            this%co(i)%r(j,j)=r_default
        enddo
    enddo
    return

    end subroutine set_diagonal_r


    !*************************************************************************
    subroutine gh5_wrt_wh_matrix_list(apath, a)
    character(*),intent(in)::apath
    complex(q),target,intent(in)::a(this%na2112)
      
    integer i,na2,na22,ibase
    complex(q),target::a_buf(this%na2max**2)
    complex(q),pointer::p_a(:,:)
      
    ibase=0
    do i = 1,this%num_imp
        na2=this%na2_list(i)
        na22=na2**2
        p_a(1:na2,1:na2)=>a_buf(1:na2**2)
        a_buf=a(ibase+1:ibase+na22)
        p_a=transpose(p_a)
        call gh5_write(p_a,na2,na2,&
                &'/impurity_'//trim(int_to_str(i-1))//apath,gh5%f_id)
        ibase=ibase+na22
    enddo
    nullify(p_a)
    return
      
    end subroutine gh5_wrt_wh_matrix_list


    subroutine calc_isimix(io,mode)
    integer,intent(in)::io,mode

    integer i,imap

    do i=1,this%num_imp
        imap=this%imap_list(i)
        if(imap==i)then
            call calc_co_isimix(this%co(i),mode)
        else
            this%co(i)%isimix=this%co(imap)%isimix
        endif
    enddo
    if(io>0)then
        call output_matrices('isimix-var',this%isimix,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
    endif
    return

    end subroutine calc_isimix


    subroutine eval_sl_vec_all(mode,io)
    integer,intent(in)::mode,io

    integer i
    character name_*12

    if(.not.associated(this%sx))return
    if(mode==1)then
        name_='var-hf-only'
    else
        name_='physical'
    endif
    do i=1,this%num_imp
        call eval_co_sl_vec(this%co(i),mode)
        if(io>0)then
            write(io,'(" imp =",i4," s_xyz(",a12,") =",3f12.5)')i,name_, &
                &this%co(i)%s_val(:,mode)
            write(io,'(10x," l_xyz(",a12,") =",3f12.5)')name_, &
                &this%co(i)%l_val(:,mode)
        endif
    enddo
    return

    end subroutine eval_sl_vec_all


    subroutine gh5_read_wh_h1e(nspin_in)
    integer,intent(in)::nspin_in

    integer i,naso,na22,ibase
    complex(q),allocatable::h1e(:,:,:,:)

    this%h1e=0
    allocate(h1e(this%nasomax,this%nasomax,this%num_imp,max(nspin_in/this%iso,1)))
    call gh5%read(h1e(:,:,:,1),this%nasomax,this%nasomax,this%num_imp, &
            &"/ispin_0/h1e_list",gh5%f_id)
    if(this%iso==1.and.nspin_in==2)then
        call gh5%read(h1e(:,:,:,2),this%nasomax,this%nasomax,this%num_imp, &
                &"/ispin_1/h1e_list",gh5%f_id)
    endif

    do i=1,this%num_imp
        naso=this%naso_imp(i)
        ! c-order to fortran-order
        h1e(:,:,i,1)=transpose(h1e(:,:,i,1))
        this%co(i)%h1e(1:naso,1:naso)=h1e(1:naso,1:naso,i,1)
        if(this%iso==1)then
            if(nspin_in==2)then
                h1e(:,:,i,2)=transpose(h1e(:,:,i,2))
                this%co(i)%h1e(1+naso:,1+naso:)=h1e(1:naso,1:naso,i,2)
            else
                this%co(i)%h1e(1+naso:,1+naso:)=h1e(1:naso,1:naso,i,1)
            endif
        endif
    enddo
    deallocate(h1e)
    return
      
    end subroutine gh5_read_wh_h1e


    subroutine gh5_read_loc_matrix_list(this, gh5, path, dim_imp, a)
    class(localstore_ob)::this
    class(hdf5_ob)::gh5
    character(*),intent(in)::path
    integer,intent(in)::dim_imp(*)
    complex(q),target,intent(out)::a(*)
      
    integer i,na2,na22,ibase
    complex(q),pointer::p_a(:,:)
      
    ibase=0
    do i = 1,this%num_imp
        na2=dim_imp(i)
        na22=na2**2
        p_a(1:na2,1:na2)=>a(ibase+1:ibase+na22)
        call gh5%read(p_a,na2,na2,'/impurity_'//trim(int_to_str(i-1))// &
                &path,gh5%f_id)
        p_a(1:na2,1:na2)=transpose(p_a(1:na2,1:na2))
        ibase=ibase+na22
    enddo
    nullify(p_a)
    return
      
    end subroutine gh5_read_loc_matrix_list
    

    !*************************************************************************
    subroutine gh5_wrt_wh_rl(fname)
    character(*) fname

    call gh5%open_w(fname, gh5%f_id)
    call gh5_create_impurity_groups(gh5%f_id)
    call gh5_wrt_wh_matrix_list('/r',this%r)
    call gh5_wrt_wh_matrix_list('/lambda',this%la1)
    call gh5%close(gh5%f_id)
    return
      
    end subroutine gh5_wrt_wh_rl
      
      
    !*************************************************************************
    subroutine gh5_read_wh_rl()
    logical lexist
    integer err

    call gh5%open_r('WH_RL_Inp.h5', gh5%f_id)
    call gh5_read_loc_matrix_list('/lambda',this%na2_list,this%la1)
    call h5lexists_f(gh5%f_id,'/impurity_0/r',lexist,err)
    if(lexist)then
        call gh5_read_loc_matrix_list('/r',this%na2_list,this%r)
    else
        call set_diagonal_r(0.999_q)
    endif
    call gh5%close(gh5%f_id)
    return
      
    end subroutine gh5_read_wh_rl
    

    !*************************************************************************
    ! read [s_x, s_y, s_z] and [l_x, l_y, l_z]
    !*************************************************************************
    subroutine set_loc_sl_vec(this,gh5)
    class(localstore_ob)::this
    class(hdf5_ob)::gh5

    integer i,err
    logical lexist
   
    call h5lexists_f(gh5%f_id,'/impurity_0/sz',lexist,err)
    if(lexist)then
        allocate(this%sz(this%na2112))
        call gh5_read_loc_matrix_list('/sz',this%na2_list,this%sz)
    endif
    call h5lexists_f(gh5%f_id,'/impurity_0/lz',lexist,err)
    if(lexist)then
        allocate(this%lz(this%na2112))
        call gh5_read_loc_matrix_list('/lz',this%na2_list,this%lz)
    endif
    call h5lexists_f(gh5%f_id,'/impurity_0/sx',lexist,err)
    if(lexist)then
        allocate(this%sx(this%na2112),this%sy(this%na2112), &
                &this%lx(this%na2112),this%ly(this%na2112))
        call gh5_read_loc_matrix_list('/sx',this%na2_list,this%sx)
        call gh5_read_loc_matrix_list('/sy',this%na2_list,this%sy)
        call gh5_read_loc_matrix_list('/lx',this%na2_list,this%lx)
        call gh5_read_loc_matrix_list('/ly',this%na2_list,this%ly)
    endif
    return
      
    end subroutine set_loc_sl_vec
    

    subroutine gh5_read_loc_hs(this, gh5, apath, hpath, spath, mb)
    class(localstore_ob)::this
    class(hdf5_ob)::gh5
    character(*),intent(in)::apath, hpath, spath
    type(matrix_basis),intent(inout)::mb

    integer na2,dim_hs,na223,i,j,ibase,jbase
    complex(q),pointer::mb_hs(:,:,:)
    integer,pointer::mb_m_struct(:,:)
    
    allocate(mb%dim_hs(this%num_imp))
    call gh5%read(mb%dim_hs, this%num_imp, apath, gh5%group_id, gh5%f_id)
    call set_loc_hs_dimtot(this,mb)
    allocate(mb%hs(mb%dimhs1123),mb%m_struct(this%na2112))
    mb%m_struct=0
    ibase=0; jbase=0
    do i=1,this%num_imp
        na2=this%na2_list(i)
        dim_hs=mb%dim_hs(i)
        na223=na2**2*dim_hs
        if(na223>0)then
            mb_hs(1:na2,1:na2,1:dim_hs)=>mb%hs(ibase+1:ibase+na223)
            call gh5%read(mb_hs,na2,na2,dim_hs,'/impurity_'// &
                    &trim(int_to_str(i-1))//hpath,gh5%f_id)
            ! switch axis due to c-fortran convention.
            do j=1,dim_hs
                mb_hs(:,:,j)=transpose(mb_hs(:,:,j))
            enddo
            ibase=ibase+na223
            mb_m_struct(1:na2,1:na2)=>mb%m_struct(jbase+1:jbase+na2**2)
            call gh5%read(mb_m_struct,na2,na2,'/impurity_'// &
                    &trim(int_to_str(i-1))//spath,gh5%f_id)
            ! switch axis due to c-fortran convention.
            mb_m_struct=transpose(mb_m_struct)
        endif
        jbase=jbase+na2**2
    enddo
    nullify(mb_hs,mb_m_struct)
    return
      
    end subroutine gh5_read_loc_hs
     

    !*************************************************************************
    subroutine set_loc_hs(this,gh5,dpath,hpath,spath,mb,lopen)
    class(localstore_ob)::this
    class(hdf5_ob)::gh5
    character(*),intent(in) :: dpath,hpath,spath
    type(matrix_basis),intent(inout)::mb
    logical,intent(in)::lopen

    logical lexist
    integer err
     
    if(lopen)then
        call h5lexists_f(gh5%f_id,dpath,lexist,err)
    else
        lexist=.false.
    endif
    if(lexist)then
        call gh5_read_loc_hs(dpath,hpath,spath,mb)
    else
        mb%dim_hs=>this%hm%dim_hs
        mb%hs=>this%hm%hs
        mb%dimhsmax=this%hm%dimhsmax
        mb%dimhst=this%hm%dimhst
        mb%dimhs1123=this%hm%dimhs1123
    endif
    return
      
    end subroutine set_loc_hs


    ! Convention: 
    ! {{complext spherical harmonics (CSH)}_dn,up}
    ! to symmetry adapted basis (SAB)
    subroutine set_loc_db2sab(this,gh5,io)
    class(localstore_ob)::this
    class(hdf5_ob)::gh5
    integer,intent(in)::io

    logical lexist
    integer err
   
    call h5lexists_f(gh5%f_id,'/impurity_0/db2sab',lexist,err)
    if(lexist)then
        allocate(this%db2sab(this%na2112))
        call gh5_read_loc_matrix_list(this,gh5,'/db2sab',this%na2_list, &
                &this%db2sab)
        if(io>0)then
            write(io,'(" transformation db_to_sab read in.")')
        endif
    endif
    return
      
    end subroutine set_loc_db2sab
     

    ! read in a list of local external potentials.
    subroutine set_loc_vext(this,gh5,io)
    class(localstore_ob)::this
    class(hdf5_ob)::gh5
    integer,intent(in)::io

    allocate(this%vext(this%na2112))
    call gh5_read_loc_matrix_list(this,gh5,'/vext',this%na2_list,this%vext)
    if(io>0)then
        write(io,'(" list of local vext read in.")')
        call output_matrices('v_ext',this%vext,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
    endif
    return
 
    end subroutine set_loc_vext


    !*************************************************************************
    ! Mott localized spin-orbital information
    !*************************************************************************
    subroutine set_mott_localized_orbitals(this,gh5,lopen)
    class(localstore_ob)::this
    class(hdf5_ob)::gh5
    logical,intent(in)::lopen

    integer i,i_,err
    logical lexist

    allocate(this%fz(this%num_imp))
    if(lopen)then
        call h5lexists_f(gh5%f_id,'/impurity_0',lexist,err)
    else
        lexist=.false.
    endif
    if(.not.lexist)then
        this%fz(:)%nsorb=0; this%fz(:)%nelect=0
    else
        do i=1,this%num_imp
            call gh5%gopen('/impurity_'//trim(int_to_str(i-1)),gh5%f_id, &
                    &gh5%group_id)
            call gh5%read(this%fz(i)%nsorb,'num_mott_orbitals', &
                    &gh5%group_id,gh5%f_id)
            call gh5%read(this%fz(i)%nelect,'num_mott_electrons', &
                    &gh5%group_id,gh5%f_id)
            if(this%fz(i)%nsorb>0)then
                allocate(this%fz(i)%idx_orb(this%fz(i)%nsorb))
                call gh5%read(this%fz(i)%idx_orb,this%fz(i)%nsorb, &
                        &'mott_orbital_indices', gh5%group_id, gh5%f_id)
            endif
            call gh5%gclose(gh5%group_id)
        enddo
    endif
    return
      
    end subroutine set_mott_localized_orbitals
    

    !*************************************************************************
    subroutine output_mott_localized_orbitals(this,io)
    class(localstore_ob)::this
    integer,intent(in)::io
      
    integer i
      
    if(io<0)return
    write(io,'(" mott localized orbital info:")')
    do i=1,this%num_imp
        write(io,'(" i=",i3," nsorb=",i2," nelect=",i2)')i, &
                &this%fz(i)%nsorb,this%fz(i)%nelect
        if(this%fz(i)%nsorb>0)then
            write(io,'("    idx_orb=",14i3)')this%fz(i)%idx_orb
        endif
    enddo
    return
      
    end subroutine output_mott_localized_orbitals
    

    !*************************************************************************
    subroutine modify_r_la1_frozen(mode,val)
    integer,intent(in)::mode
    real(q),intent(in)::val

    integer i,j,j_
      
    do i=1,this%num_imp
        do j=1,this%fz(i)%nsorb
            j_=this%fz(i)%idx_orb(j)
            this%co(i)%r(j_,:)=0
            this%co(i)%la1(j_,:)=0; this%co(i)%la1(:,j_)=0
            if(mode>0)then
                this%co(i)%la1(j_,j_)=val
            endif
        enddo
    enddo
    return
      
    end subroutine modify_r_la1_frozen


    !*************************************************************************
    !< post processing: print and symmetizations.
    !*************************************************************************
    subroutine calc_nks_pp(io,lchk)
    integer,intent(in)::io
    logical,optional,intent(in)::lchk

    real(q) maxerr

    if(io>0)then
        call output_matrices('nks-unsym',this%nks,this%na2112,this%num_imp, &
                &this%na2_list,io,1)
    endif
    this%copy=this%nks
    call symm_dm_across_atoms(this%nks)
    ! Get expansion coefficients only
    call hm_expand_all_herm(this%nks,this%nks_coef,this%hm_l,1,.true.)
    ! Symmetrization (note non-zero diagonals)
    call hm_expand_all_sym(this%nks,this%hm,.true.,.true.)
    maxerr=maxval(abs(this%nks-this%copy))
    this%symerr=max(this%symerr,maxerr)
    if(io>0)then
        call output_matrices('nks-sym',this%nks,this%na2112,this%num_imp, &
                &this%na2_list,io,1)
        write(io,'(" max error due to symmetrization = ",f12.6)') maxerr
    endif
    call chk_eigens_matrix_list('nks',this%nks,this%na2112,this%num_imp, &
            &this%na2_list,io,0)
    call calc_nks_tot(io)
    if(.not.present(lchk))then
        call chk_nks_tiny()
    endif
    return

    end subroutine calc_nks_pp


    subroutine symm_dm_across_atoms(dm)
    complex(q),intent(inout)::dm(this%na2112)

    integer i,j,isum,nbase,na22
    complex(q) dms(this%na2max*this%na2max)

    dms=0; isum=0; nbase=0
    do i=1,this%num_imp
        na22=this%na2_list(i)**2
        nbase=nbase+na22
        if(isum==0)then
            dms(1:na22)=dm(nbase-na22+1:nbase)
        else
            dms(1:na22)=dms(1:na22)+dm(nbase-na22+1:nbase)
        endif
        isum=isum+1
        if(i<this%num_imp)then
            if(this%imap_list(i)==this%imap_list(i+1))cycle
        endif
        if(isum>1)then
            dms=dms/isum
            do j=1,isum
                dm(nbase-j*na22+1:nbase-(j-1)*na22)=dms(1:na22)
            enddo
        endif
        isum=0; dms=0
    enddo

    end subroutine symm_dm_across_atoms


    subroutine calc_nks_tot(io)
    integer,intent(in)::io

    integer i
    real(q) res

    do i=1,this%num_imp
        if(this%ispin_in==1)then
            ! case of lda
            res = trace_a(this%co(i)%nks,this%co(i)%dim2)
            this%co(i)%net=res/2
        else
            ! case of lsda
            call calc_co_net(this%co(i))
        endif
    enddo
    if(io>0)then
        write(io,'(" nele_loc total:")')
        write(io,'(4x,2(2f10.5,3x))')(this%co(i)%net,i=1,this%num_imp)
    endif
    return

    end subroutine calc_nks_tot


    subroutine calc_ncvar_pp(io)
    integer,intent(in)::io

    real(q) maxerr

    if(io>0)then
        call output_matrices('ncv-unsym',this%nc_var,this%na2112,this%num_imp, &
                &this%na2_list,io,1)
    endif
    this%copy=this%nc_var
    ! Get expansion coefficients only
    call hm_expand_all_herm(this%nc_var,this%ncv_coef,this%hm_l,1,.true.)
    ! Symmetrization (note non-zero diagonals)
    call hm_expand_all_sym(this%nc_var,this%hm,.true.,.true.)
    maxerr=maxval(abs(this%nc_var-this%copy))
    this%symerr=max(this%symerr,maxerr)
    if(io>0)then
        call output_matrices('ncv-sym',this%nc_var,this%na2112,this%num_imp, &
                &this%na2_list,io,1)
        write(io,'(" max error due to symmetrization = ",f12.6)')maxerr
    endif
    return

    end subroutine calc_ncvar_pp


    subroutine chk_nks_tiny()
    integer i,i1
    real(q) res
    real(q),parameter::small=1.e-6_q

    do i=1,this%num_imp
        do i1=1,this%co(i)%dim2
            res=real(this%co(i)%nks(i1,i1),q)
            if(res<small)then
                write(0,'(" warning in chk_nks_tiny: too small diagonal &
                        &elements",2f16.5)')this%co(i)%nks(i1,i1)
            endif
            this%co(i)%nks(i1,i1)=max(real(this%co(i)%nks(i1,i1),q),small)
        enddo
    enddo
    return

    end subroutine chk_nks_tiny


    subroutine calc_ncphy_pp(io)
    integer,intent(in)::io

    integer i
    real(q) maxerr

    if(io>0)then
        call output_matrices('ncp-unsym',this%nc_phy,this%na2112,this%num_imp, &
                &this%na2_list,io,1)
    endif
    this%copy=this%nc_phy
    call hm_expand_all_sym(this%nc_phy,this%hm,.true.,.true.)
    maxerr=maxval(abs(this%nc_phy-this%copy))
    this%symerr=max(this%symerr,maxerr)
    if(io>0)then
        call output_matrices('ncp-sym',this%nc_phy,this%na2112,this%num_imp, &
                &this%na2_list,io,1)
        write(io,'(" max error due to symmetrization = ",f12.6)')maxerr
    endif
    call renorm_ncphy(io)
    if(io>0)then
        call output_matrices('ncp-renorm',this%nc_phy,this%na2112,this%num_imp, &
                &this%na2_list,io,1)
    endif
    return

    end subroutine calc_ncphy_pp


    ! for this%h1e, etc.
    subroutine calc_herm_matrices_pp(matrices,sname,mb,ltrans,io,mode)
    complex(q),intent(inout)::matrices(this%na2112)
    character,intent(in)::sname*3
    type(matrix_basis),intent(in)::mb
    logical,intent(in)::ltrans
    integer,intent(in)::io,mode

    real(q) maxerr

    if(io>0)then
        call output_matrices(sname//'-unsym',matrices,this%na2112,this%num_imp, &
                &this%na2_list,io,mode)
    endif
    this%copy=matrices
    call symm_dm_across_atoms(matrices)
    call hm_expand_all_sym(matrices,mb,ltrans,.true.)
    maxerr=maxval(abs(matrices-this%copy))
    this%symerr=max(this%symerr,maxerr)
    if(io>0)then
        call output_matrices(sname//'-sym',matrices,this%na2112,this%num_imp, &
                &this%na2_list,io,mode)
        write(io,'(" max error due to symmetrization = ",f12.6)')maxerr
    endif
    return

    end subroutine calc_herm_matrices_pp


    subroutine calc_r01_pp(io)
    integer,intent(in)::io

    integer i

    ! r0 -> r
    forall(i=1:this%num_imp)this%co(i)%r=matmul(transpose(this%co(i)%isimix), &
            &this%co(i)%r0)

    if(io>0)then
        call output_matrices('r0-out',this%r0,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
    endif

    call  calc_r_pp(io,'r-out')

    ! r-> z=r^\dagger r
    do i=1,this%num_imp
        call annxb('c',this%co(i)%r,this%co(i)%r,this%co(i)%z, &
                &this%na2_list(i),this%na2_list(i))
    enddo

    if(io>0)then
        call output_matrices('z-out-sym',this%z,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
    endif

    call chk_eigens_matrix_list('z',this%z,this%na2112,this%num_imp, &
            &this%na2_list,io,0)
    return

    end subroutine calc_r01_pp


    subroutine calc_r_pp(io,sname)
    integer,intent(in)::io
    character,intent(in)::sname*5

    integer i
    real(q) maxerr

    if(io>0)then
        call output_matrices(sname,this%r,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
    endif
    this%copy=this%r
    call hm_expand_all_general(this%r,this%r_coef,this%hm_r,0,.false.)
    maxerr=maxval(abs(this%r-this%copy))
    this%symerr=max(this%symerr,maxerr)
    if(io>0)then
        call output_matrices(sname//'-sym',this%r,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
        write(io,'(" max error due to symmetrization = ",f12.6)')maxerr
    endif
    return

    end subroutine calc_r_pp


    subroutine calc_la1_pp(io,sname)
    integer,intent(in)::io
    character,intent(in)::sname*7

    integer i
    real(q) maxerr

    if(io>0)then
        call output_matrices(sname,this%la1,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
    endif
    this%copy=this%la1
    call hm_expand_all_herm(this%la1,this%la1_coef,this%hm_l,0,.false.)
    maxerr=maxval(abs(this%la1-this%copy))
    this%symerr=max(this%symerr,maxerr) 
    if(io>0)then
        call output_matrices(sname//'-sym',this%la1,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
        write(io,'(" max error due to symmetrization = ",f12.6)')maxerr
    endif
    return

    end subroutine calc_la1_pp


    subroutine calc_lambdac(io)
    integer,intent(in) :: io

    integer i

    do i=1,this%num_imp
        call calc_co_lambdac(this%co(i))
    enddo

    if(io>0)then
        call output_matrices('la2',this%la2,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
    endif
    return

    end subroutine calc_lambdac


    !*************************************************************************
    subroutine calc_da0_pp(io)
    integer,intent(in)::io

    integer i
    real(q) maxerr

    if(io>0)then
        call output_matrices('d0-unsym',this%d0,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
    endif
    this%copy=this%d0
    call symm_dm_across_atoms(this%d0)
    call hm_expand_all_sym(this%d0,this%hm_r,.false.,.false.)
    maxerr=maxval(abs(this%d0-this%copy))
    this%symerr=max(this%symerr,maxerr)
    if(io>0)then
        call output_matrices('d0-sym',this%d0,this%na2112,this%num_imp, &
                &this%na2_list,io,0)
        write(io,'(" max error due to symmetrization = ",f12.6)')maxerr
    endif
    return

    end subroutine calc_da0_pp


    !*************************************************************************
    subroutine calc_da(io)
    integer,intent(in)::io

    call calc_da0_pp(io)
    call d0_to_d()
    if(io>0)then
       call output_matrices('d-sym',this%d,this%na2112,this%num_imp, &
            &this%na2_list,io,0)
    endif
    return

    end subroutine calc_da


    !*************************************************************************
    !< mode>0: get c; mode<0: get a; mode=0: symmetrization (get c then a)
    !*************************************************************************
    subroutine hm_expand_all_herm(a,c,mb,mode,ltrans)
    type(matrix_basis),intent(in)::mb
    complex(q),target,intent(inout)::a(this%na2112)
    real(q),intent(inout)::c(mb%dimhst)
    integer,intent(in)::mode
    logical,intent(in)::ltrans

    integer i,ibase,imap,abase,hsbase,dim_hs,na2,na22,ibase_list(this%num_imp)
    complex(q),pointer::p_a(:,:),p_hs(:,:,:)
    complex(q) c1(mb%dimhsmax)

    ibase=0
    abase=0
    hsbase=0
    do i=1,this%num_imp
        na2=this%na2_list(i)
        na22=na2**2
        dim_hs=mb%dim_hs(i)
        imap=this%imap_list(i)
        if(imap==i)then
            ibase_list(i)=ibase
        else
            ibase_list(i)=ibase_list(imap)
        endif
        p_a(1:na2,1:na2)=>a(abase+1:abase+na22)
        if(dim_hs>0)then
            p_hs(1:na2,1:na2,1:dim_hs)=>mb%hs(hsbase+1:hsbase+na22*dim_hs)
            if(mode<=0)then
                c1(1:dim_hs)=c(ibase_list(i)+1:ibase_list(i)+dim_hs)
            endif
            call get_hm_expand(p_a,p_hs,na2,dim_hs,c1(1:dim_hs), &
                    &mode,ltrans,.true.)
            if(imap==i)then
                if(mode>=0)then
                    c(ibase+1:ibase+dim_hs)=c1(1:dim_hs)
                endif
                ibase=ibase+dim_hs
            endif
        endif
        abase=abase+na22
        hsbase=hsbase+na22*dim_hs
    enddo
    return

    end subroutine hm_expand_all_herm


    subroutine hm_expand_all_general(a,c,mb,mode,ltrans)
    type(matrix_basis),intent(in)::mb
    complex(q),target,intent(inout)::a(this%na2112)
    complex(q),intent(inout)::c(mb%dimhst)
    integer,intent(in)::mode
    logical,intent(in)::ltrans

    integer i,imap,ibase,abase,hsbase,nbase,dim_hs,na2,na22
    complex(q),pointer::p_a(:,:),p_hs(:,:,:)

    ibase=0
    abase=0
    hsbase=0
    do i=1,this%num_imp
        na2=this%na2_list(i)
        na22=na2**2
        dim_hs=mb%dim_hs(i)
        if(dim_hs>0)then
            p_a(1:na2,1:na2)=>a(abase+1:abase+na22)
            p_hs(1:na2,1:na2,1:dim_hs)=>mb%hs(hsbase+1:hsbase+na22*dim_hs)
            imap=this%imap_list(i)
            if(imap==i)then
                call get_hm_expand(p_a,p_hs,na2,dim_hs, &
                        &c(ibase+1:ibase+dim_hs),mode,ltrans,.false.)
                ibase=ibase+dim_hs
            elseif(mode<=0)then
                nbase=sum(this%na2_list(1:imap-1)**2)
                a(abase+1:abase+na22)=a(nbase+1:nbase+na22)
            endif
        elseif(mode<0)then
            a(abase+1:abase+na22)=0
        endif
        abase=abase+na22
        hsbase=hsbase+na22*dim_hs
    enddo
    return

    end subroutine hm_expand_all_general


    subroutine hm_expand_all_sym(a,mb,ltrans,lherm)
    complex(q),target,intent(inout)::a(this%na2112)
    type(matrix_basis),intent(in)::mb
    logical,intent(in)::ltrans,lherm

    integer i,abase,hsbase,dim_hs,na2,na22
    complex(q),pointer::p_a(:,:),p_hs(:,:,:)

    abase=0
    hsbase=0
    do i=1,this%num_imp
        na2=this%na2_list(i)
        na22=na2**2
        dim_hs=mb%dim_hs(i)
        if(dim_hs>0)then
            p_a(1:na2,1:na2)=>a(abase+1:abase+na22)
            p_hs(1:na2,1:na2,1:dim_hs)=>mb%hs(hsbase+1:hsbase+na22*dim_hs)
            call get_hm_expand(p_a,p_hs,na2,dim_hs,ltrans,lherm)
        endif
        abase=abase+na22
        hsbase=hsbase+na22*dim_hs
    enddo
    return

    end subroutine hm_expand_all_sym


    !*************************************************************************
    ! renormalize nc_phy
    !*************************************************************************
    subroutine renorm_ncphy(io)
    integer,intent(in)::io

    integer i
    real(q) sum_ks,sum_phy,sum_var

    do i=1,this%num_imp
        sum_ks =trace_a(this%co(i)%nks   , this%co(i)%dim2)
        sum_phy=trace_a(this%co(i)%nc_phy, this%co(i)%dim2)
        sum_var=trace_a(this%co(i)%nc_var, this%co(i)%dim2)
        if(io>0)then
            write(io,'(" imp=",i3," sum_phy-sum_var=",f16.8)') &
                    &i,sum_phy-sum_var
            write(io,'(7x       ," sum_phy-sum_ks =",f16.8, &
                    &" would be renormalized!")')sum_phy-sum_ks
        endif
        if(sum_phy<1.e-16_q)then
            this%co(i)%nc_phy = 0._q
        else
            this%co(i)%nc_phy=this%co(i)%nc_phy/sum_phy*sum_ks ! renormalized
        endif
    enddo
    return

    end subroutine renorm_ncphy


    !*************************************************************************
    subroutine d0_to_d()
    integer i

    do i=1,this%num_imp
        call co_d0_to_d(this%co(i))
    enddo
    return

    end subroutine d0_to_d


    subroutine gh5_create_impurity_groups(fid)
    integer(hid_t),intent(in)::fid

    integer i

    if(fid<0)return
    do i = 1, this%num_imp
        call gh5_gcreate("/impurity_"//trim(int_to_str(i-1)),gh5%f_id)
    enddo
    return

    end subroutine gh5_create_impurity_groups


    subroutine calc_et1_list()
    integer i

    do i=1,this%num_imp
        this%et1(i)=sum(this%co(i)%h1e*this%co(i)%nc_phy)
    enddo
    return

    end subroutine calc_et1_list


    subroutine calc_eu2_hf_list()
    integer i

    do i=1,this%num_imp
        call calc_co_eu2_hf(this%co(i), this%eu2(i))
    enddo
    return

    end subroutine calc_eu2_hf_list


    subroutine output_energies_wh(io)
    integer,intent(in)::io

    integer i

    write(io,'(" impurity-wise interaction energy:")')
    write(io,'(4x,5f12.5)')(this%eu2(i), i=1,this%num_imp)
    write(io,'(" total impurity interaction energy = ",f0.7)') &
            &sum(this%eu2)
    write(io,'(" impurity-wise one-body part energy:")')
    write(io,'(4x,5f12.5)')(this%et1(i), i=1,this%num_imp)
    write(io,'(" total impurity one-body part energy = ",f0.7)') &
            &sum(this%et1)
    write(io,'(" total lambda-dc energy = ",f0.7)') this%edcla1
    return

    end subroutine output_energies_wh



end module localstore
