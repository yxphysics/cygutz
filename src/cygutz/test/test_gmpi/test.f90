program test_gmpi
use gmpi
implicit none
integer::i=1
complex(8)::zbuf(2,2,1)=1.d0
type(mpi_ob)::mpi

call mpi%init()
call mpi%sum_all(i)
write(*,*)mpi%myrank,i
! cheat the compiler a bit, only address is needed.
call mpi%sum_all(zbuf(:,1,1),4)
write(*,*)zbuf
call mpi%finalize()


end program test_gmpi
