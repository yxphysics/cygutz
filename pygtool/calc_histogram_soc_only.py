#!/usr/bin/env python3

'''Script works for both metallic phase and Mott phase with
spin-orbit interaction ONLY.'''

from pygrisb.mbody.multiplets_analysis_soc import driver

driver()
