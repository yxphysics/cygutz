    program test_parallel_hdf5
    use gprec
    use gmpi
    use ghdf5
    implicit none
    integer,parameter :: n3t=286
    integer :: i,n1,n3,nres,n3base,ierr
    complex(q),allocatable :: zbuf(:,:,:)
    type(hdf5_ob) gh5
    type(mpi_ob) mpi

    call mpi%init()
    n1=26
    n3=n3t/mpi%nprocs
    nres=n3t-n3*mpi%nprocs
    if(mpi%myrank<nres)then
        n3=n3+1
    endif
    n3base=n3*mpi%myrank
    if(nres>0.and.mpi%myrank>=nres)then
        n3base=n3base+min(mpi%myrank,nres)
    endif
    allocate(zbuf(n1,n1,n3))
    ! initialize array
    do i=1,n3
        zbuf(:,:,i)=i+n3base
    enddo
    write(0,*)n3,n3base

    call gh5%init()

    ! write file
    call gh5%fopen("testPara.h5", 1, "w")
    call gh5%dcreate(3,(/n1,n1,n3t/),'/array3',gh5%dcomplex_id,1,1)
    call gh5%dwrite(zbuf,n1,n1,n3,0,0,n3base,1,serialio=.true.)
    call gh5%dclose(1)
    call gh5%fclose(1)

    zbuf=0
    ! read file
    call gh5%fopen("testPara.h5", 1, "r")
    call gh5%dopen('/array3', 1, 1)
    if(mpi%myrank/=3)then
        ! independent io
        call gh5%read(zbuf,n1,n1,n3,0,0,n3base,1,serialio=.true.)
        ! collective io
        ! call gh5%read(zbuf,n1,n1,n3,0,0,n3base,1)
    endif
    call gh5%dclose(1)
    call gh5%fclose(1)
    write(*,*)zbuf(n1,n1,n3)

    call gh5%end()
    call mpi%finalize()

    end program test_parallel_hdf5
